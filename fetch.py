import sys
import asyncio
import aiohttp
import aiofiles
from bs4 import BeautifulSoup
import time
import json

async def fetch_url(session, url):
    '''Fetch the url and save the html.

     Fetch and write are made together because this is faster than fetching everything and then writing everything.
     '''
    try:
        async with session.get(url, raise_for_status=True) as response:
            html = await response.text()
        await write_file(f"{trunc_name(url)}.html", html)
        return url, html

    except Exception as e:
        print(f"Fail to fetch {url} : {e}")

def trunc_name(url):
    return url.replace('https://', '').replace('http://', '')


async def save_metadata(url, html):
    '''Save the metadata for the html file to disk'''
    soup = BeautifulSoup(html, features="html.parser")
    name = trunc_name(url)
    metadata = {
        "site": name,
        "num_links": len(soup.find_all('a')),
        "images": len(soup.find_all('img')),
        "last_fetch": time.strftime("%a %b %d %Y %H:%M UTC", time.gmtime()),
    }
    await write_file(f"{name}.json", json.dumps(metadata))

async def write_file(name, content):
    try:
        async with aiofiles.open(name, mode='w', encoding='utf-8') as f:
            return await f.write(content)
    except Exception as e:
        print(f"Fail to write the content of {name} to the disk : {e}")

async def fetch_all(loop, urls):
    '''Fetch all the html file and the metadata.'''
    fetch_tasks = []
    async with aiohttp.ClientSession() as session:
        for url in urls:
            fetch_tasks.append(loop.create_task(fetch_url(session, url)))
        results = await asyncio.gather(*fetch_tasks)

    # The parsing might take some time, so it is made after in other to not block the other async requests.
    parse_tasks = []
    for res in results:
        if res: # is None if an exception has been raised
            parse_tasks.append(loop.create_task(save_metadata(*res)))
    await asyncio.gather(*parse_tasks)

def main(urls):
    '''Start and run the asyncio loop.'''
    loop = asyncio.new_event_loop()
    try:
        asyncio.set_event_loop(loop)
        loop.run_until_complete(fetch_all(loop, urls))
    finally:
        loop.close()


def read_metadata(url):
    '''Read the metadata of the given link.
    As there is only one url, there is no need for asynchronous code here.
    '''
    try:
        with open(f"{trunc_name(url)}.json", 'r') as file:
            metadata = json.load(file)
        print('\n'.join([f"{key}: {value}" for key, value in metadata.items()]))
        return metadata
    except Exception as e:
        print(f"Fail to read the metadata of {url} : {e}")

if __name__ == '__main__':
    if len(sys.argv) == 3 and sys.argv[1] == '--metadata':
        read_metadata(sys.argv[2])
    else:
        main(sys.argv[1:] + ["https://www.google.com"])
