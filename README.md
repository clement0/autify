# Documentation

To build the container, run:  
`docker build -t project:1.0 .`

To run the container, run:  
`docker run -it project:1.0 sh`

Then, to fetch one or multiple urls, run:  
`python ./fetch.py https://www.google.com`  
or  
`python ./fetch.py https://www.google.com https://autify.com`  
*Note: The html files will be stored in the current folder.*  

To print the metadata of one url, run:  
`python ./fetch.py --metadata https://www.google.com`  

To run some tests, run:
`python ./test.py`  

To quit the program, run:  
`exit`

# Improvement

One Improvement would be to set up a database to store the metadata. Given the nature of the data and the high ratio of writes/reads, I would choose a document-oriented database such as MongoDB.
