FROM python:latest
LABEL Maintainer="Clement Guyomard"
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
COPY fetch.py ./
COPY test.py ./