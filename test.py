import unittest
import os
from fetch import main, trunc_name, read_metadata


class TestSum(unittest.TestCase):

    def test_one_link(self):
        url = 'https://www.yahoo.com'
        main([url])
        self.assertTrue(os.path.isfile(f"{trunc_name(url)}.html"))
        self.assertTrue(os.path.isfile(f"{trunc_name(url)}.json"))
        os.remove(f"{trunc_name(url)}.html")
        os.remove(f"{trunc_name(url)}.json")

    def test_three_links(self):
        urls = ['https://www.google.com', 'https://autify.com', 'https://www.wikipedia.org']
        main(urls)
        for url in urls:
            self.assertTrue(os.path.isfile(f"{trunc_name(url)}.html"))
            self.assertTrue(os.path.isfile(f"{trunc_name(url)}.json"))
            os.remove(f"{trunc_name(url)}.html")
            os.remove(f"{trunc_name(url)}.json")

    def test_wrong_links(self):
        urls = ['hps://www.google.com', 'https://autifytest.com']
        main(urls)
        for url in urls:
            self.assertFalse(os.path.isfile(f"{trunc_name(url)}.html"))
            self.assertFalse(os.path.isfile(f"{trunc_name(url)}.json"))

    def test_metadata(self):
        url = 'https://www.yahoo.com'
        main([url])
        self.assertTrue(os.path.isfile(f"{trunc_name(url)}.html"))
        self.assertTrue(os.path.isfile(f"{trunc_name(url)}.json"))
        metadata = read_metadata(url)
        for key, type in [('images', int), ('num_links', int), ('site', str), ('last_fetch', str)]:
            self.assertTrue(key in metadata.keys())
            self.assertTrue(isinstance(metadata[key], type))
            self.assertTrue(metadata[key]) # test empty string or 0 links/images
        os.remove(f"{trunc_name(url)}.html")
        os.remove(f"{trunc_name(url)}.json")


if __name__ == '__main__':
    unittest.main()
